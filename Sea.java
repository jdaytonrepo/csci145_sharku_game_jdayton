import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.Random;

/**
 * Write a description of class Sea here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Sea extends World
{
    public Shark shark; // declare type of object for the variable
    public Heart heart;
    public Heart2 heart2;
    public Heart3 heart3;
    public Heart4 heart4;
    public Heart5 heart5;
    private GreenfootSound bgMusic;
    private GreenfootSound winning;
    private GreenfootSound applause;
    private int stopCounter = 0;
    public scoreRank score;
    public Counter counter;
    public Timer timer;
    
    
    
    
    private ArrayList<Test> tests;
    
    private ArrayList<Fail> fail;
    
    
    /**
     * Constructor for objects of class Sea.
     * 
     */
    public Sea()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(700, 500, 1, false); 
        shark = new Shark();
         
        addTimer();
        
        heart = new Heart(); // create variables for Heart objects for the Heart class
        heart2 = new Heart2();
        heart3 = new Heart3();
        heart4 = new Heart4();
        heart5 = new Heart5();
        counter = new Counter();
        addHeart(); // add the heart objects to the world
        bgMusic = new GreenfootSound ("cft2.mp3"); //background music
        winning = new GreenfootSound("winning.mp3");
        applause = new GreenfootSound("applause.mp3");
        addLogo2();
        addObject( shark, 450, 200);
        stopped();
        timer = new Timer();
        addCounter();
        
        
        
        //setBackground("IMG_0937.jpg");
        
     
    } // end constructor of sea
    
    public void addTimer()
    {
        addObject( new Timer(), 250, 30);
    } // end method addTimer()
    
    public void addCounter()
    {
        addObject( new Counter(), 150, 470);
    } // end method addCounter
    
    public void addAssociate()
    {
            Associate associate = new Associate();
       
            addObject( new Associate(), 50, 450);
        
    } // end method addAssociate
    
    public void addBachelor()
    {
        Bachelor bachelor = new Bachelor();
        
        addObject( new Bachelor(), 50, 450 );
    } // end method addBachelor
    
    public void addMasters()
    {
        Masters masters = new Masters();
        
        addObject( new Masters(), 50, 450 );
    } // end method addMasters
    
    public void addDoctorate()
    {
        Doctorate doctorate = new Doctorate();
        
        addObject( new Doctorate(), 50, 450);
    } // end method addDoctorate
    
    public void addLogo()
    {
        Logo logo = new Logo();
        
        addObject( new Logo(), 350, 250 );
    } // end method addLogo
    
     public void addLogo2()
    {
        Logo2 logo = new Logo2();
        
        addObject( new Logo2(), 647, 25 );
    } // end method addLogo
    
    public void addPause()
    {
        Paused pause = new Paused();
        
        addObject(new Paused(), 63, 30 );
    } // end method addPause
    
    public void addScoreRank()
    {
       scoreRank score = new scoreRank();
       addObject( new scoreRank(), 350, 250 ); 
    } // end method addScoreRank
    
    /**
     * Act method for sea class
     */
    public void act()
    {
        if ( Greenfoot.getRandomNumber(1000) < 5 ) // spawn tests 0.5% of the time
        {
            addTest();
        } // end if
        
        if  ( Greenfoot.getRandomNumber(1000) < 5 ) // spawn fails 0.5% of the time
        {
            addFail();
        } // end if
        
        if ( shark.getTestCounter() == 10 && shark.getOkToAddDegree() )
        {
            addAssociate();
            shark.setOkToAddDegree( false );
            applause.stop();
            applause.play();
        } // end if
       
        if ( shark.getTestCounter() == 20 && shark.getOkToAddDegree() )
        {
            addBachelor();
            shark.setOkToAddDegree( false );
            applause.stop();
            applause.play();
        } // end if
        
        if ( shark.getTestCounter() == 40 && shark.getOkToAddDegree() )
        {
            addMasters();
            shark.setOkToAddDegree( false );
            applause.stop();
            applause.play();
        } // end if 
        
        if ( shark.getTestCounter() == 80 && shark.getOkToAddDegree() )
        {
            addDoctorate();
            shark.setOkToAddDegree( false );
            winning.stop();
            winning.play();
        } // end if 
        
        
        
         if ( Greenfoot.isKeyDown("space") )
         {
            
           
            if ( bgMusic.isPlaying() )
            {
                // Pause the music so that when it plays again,
                // it picks up where it left off
                bgMusic.pause();
                Greenfoot.stop();
                
            }
            else
            {
                
                // if the background music isn't already 
                // playing, then start playing it
                bgMusic.playLoop();
                Greenfoot.start();
            }
           } // end if/else
            
            
         
        
        
    } // end method act
    
   
    
    
    /**
     * Add heart object to the world.
     */
    public void addHeart()
    {
       
        addObject(new Heart(), 400, 450); // add heart object
        addObject(new Heart2(), 460, 450);
        addObject(new Heart3(), 520, 450);
        addObject(new Heart4(), 580, 450);
        addObject(new Heart5(), 640, 450);
    } // end method
    
    
    /**
     * Add all test objects to the world.
     * 
     * A.K.A spawning method for Test.
     */
    public void addTest()
    {
        Test newTest = new Test(); // declare newTest of type Test
        
        // add the object to the screen somewhere above the top boundary between x = 100 and x = 500
        addObject( newTest, 100 + Greenfoot.getRandomNumber( 401 ), -100 );

    } // end method
    
    /**
     * Add all Fail objects to the world.
     * 
     * A.K.A spawning method for Fail.
     */
    public void addFail()
    {
        Fail newFail = new Fail();
        
        addObject( newFail, 100 + Greenfoot.getRandomNumber(401), -100);
    } // end method addFail
    
     /**
      * Check for tests contacting the bottom of the world.
      * 
      * Method is modified from dolphin array list file.
      */
    public void checkTestBottomEdge()
    {
       
        tests = (ArrayList<Test>)( getObjects( Test.class ) );
        
        if ( tests.size() > 0 ) 
        {
            
            // check if test has reaced the bottom
            for ( Test thisTest : tests )
            {
                
                if ( thisTest.getY() > 450 )
                {
                    removeObject( thisTest ); // remove test object from the world
                } // end if
                
            } // end for
            
        } // end if
        
    } // end method 
    
     /**
     * This method specifies what happens when we click 
     * the "pause" button in the Greenfoot window
     */
    public void stopped()
    {
        //System.out.println("Game is paused");
        if ( bgMusic.isPlaying() )
        {
            bgMusic.pause();
            
            addPause();
            
            stopCounter += 1;
        }
        
         if ( stopCounter < 1)
         {
             addLogo();
         } // end if 
         
         if ( stopCounter > 0 )
         {
        
             addScoreRank();
         } // end if
    } // end method stopped
    
    /**
     * This method specifies what happens when we click
     * the "run" button in the Greenfoot window
     */
    public void started()
    {
        //System.out.println("Game is now running");
        if ( !bgMusic.isPlaying() )
        {
            bgMusic.playLoop();
        }
       removeObjects(getObjects(Logo.class));
       removeObjects(getObjects(Paused.class));
       removeObjects(getObjects(scoreRank.class));
       // ArrayList logos = getObjects( Logo.class );
        
      //  if ( logos != null )
       // {
         //   for ( Logo thisLogo : logos )
           // {
             //   removeObject( thisLogo );
           // }
          //  }
        
        
    } // end method started
   
    
   
}
