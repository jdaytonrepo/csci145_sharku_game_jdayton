import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;



/**
 * Write a description of class Dolphin here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Shark extends Actor
{
    // instance variables and fields
    private GreenfootImage[] images = new GreenfootImage[2];
    private GreenfootSound bite;
    private GreenfootSound failBite;
    private GreenfootSound drama;
    
    private boolean directionIsRight;
    private boolean okToAddDegree = true;
    
    private int frameCount;
    private int imageIndex;
    private static int testCounter; // counter for tests consumed

    public int failCounter; // counter for fails consumed
    
    
    
    // declare reference to a SpriteSheet object
    private SpriteSheet spritesheet;
    private Shark thisShark; 
    private Logo logo;
    
    
    // constructor
    public Shark()
    {
        frameCount = 0;
        imageIndex = 0;
        testCounter = 0;
        failCounter = 0;
        
        directionIsRight = true;
        
        // instantiate the SpriteSheet object itself
        spritesheet = new SpriteSheet();
        
        
        // assign actual image objects to the reference variables
        // (which are elements of the images array)
        /*
        images[0] = new GreenfootImage("frame0.png");
        images[1] = new GreenfootImage("frame1.png");
        images[2] = new GreenfootImage("frame2.png");
        images[3] = new GreenfootImage("frame3.png");
        images[4] = new GreenfootImage("frame4.png");
        images[5] = new GreenfootImage("frame5.png");
        */
       
        // extract the first row of animation frames
       // images[0] = new GreenfootImage( spritesheet.getSprite( new GreenfootImage("Shark.png"),
                                       // 0, 0, 160, 70, 160, 70 ) );
       // images[1] = new GreenfootImage( spritesheet.getSprite( new GreenfootImage("Shark2.png"),
                                       // 160, 0, 320, 70, 160, 70 ) );
        
                                        /*images[2] = new GreenfootImage( spritesheet.getSprite( new GreenfootImage("Shark1.png"),
                                        320, 0, 480, 70, 160, 70 ) );
                                        
        // extract the second row of animation frames
        images[3] = new GreenfootImage( spritesheet.getSprite( new GreenfootImage("Shark1.png"),
                                        0, 70, 160, 140, 160, 70 ) );
        images[4] = new GreenfootImage( spritesheet.getSprite( new GreenfootImage("Shark1.png"),
                                        160, 70, 320, 140, 160, 70 ) );
        images[5] = new GreenfootImage( spritesheet.getSprite( new GreenfootImage("Shark1.png"),
                                        320, 70, 480, 140, 160, 70 ) );                                
                                        
        
      */  
     bite = new GreenfootSound("bite.mp3"); // sound from freeSFX.co.uk
     
     failBite = new GreenfootSound("fail.mp3"); // sound from freeSFX.co.uk
     drama = new GreenfootSound("drama.mp3");
     Logo logo = new Logo();
     // thisShark = new Shark();
       
    } // end constructor for Shark
    
    /**
     * Act - do whatever the Dolphin wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
      //  frameCount++;
        //System.out.println("Frame count: " + frameCount );
        
       // if ( frameCount == 5 )
       // {
           // updateImage();
          //  frameCount = 0;
       // }
       checkKeyPress();
       move(0);
       score();
       
       
       boundary();
       
         
    } 
    
    public static int getTestCounter()
    {
        return testCounter;
    }
    
    public void setTestCounter( int testCounter )
    {
        this.testCounter = testCounter;
    }
    
    public boolean getOkToAddDegree()
    {
        return okToAddDegree;
    }
    
    public void setOkToAddDegree( boolean status )
    {
        okToAddDegree = status;
    }
    
    public void checkKeyPress()
    {
        if ( Greenfoot.isKeyDown("up"))
        {
            turn(4);
             
        } // end if 
        
        if ( Greenfoot.isKeyDown("down"))
        {
            turn(-4);
           
        } // end if 
        
        if (Greenfoot.isKeyDown("left"))
        {
            move(-7);
           
        } // end if 
        
        if (Greenfoot.isKeyDown("right"))
        {
            move(7);
             
        } // end if 
    } // end method
    
    /**
     * Method for eating tests.
     * 
     * Eating tests will increase in a counter that will ultimately
     * earn degrees.
     */
    
    public void score()
    {
       
       if ( canSee(Test.class))
       {
           eat(Test.class); // remove test
           bite.stop(); // if sound is already playing then stop
           bite.play();
           setImage("bite.png");
           
           testCounter += 1; // increment testCounter
           
           setOkToAddDegree( true );
       } // end if
       else
       {
           setImage("Shark.png");
       } // end else
      
        
       if ( canSee(Fail.class))
       {
           eat(Fail.class); // remove fail
           
          // eat(Heart.class);
           
           failBite.stop();
           failBite.play();
           setImage("bite.png");
           failCounter += 1; // increment failCounter
           
           if ( failCounter == 1 )
           {
               getWorld().removeObjects(getWorld().getObjects(Heart.class)); // remove Heart class
           } // end if 
           
           if ( failCounter == 2 )
           {
               getWorld().removeObjects(getWorld().getObjects(Heart2.class));
           } // end if 
           
           if ( failCounter == 3 )
           {
               getWorld().removeObjects(getWorld().getObjects(Heart3.class));
           } // end if 
           
           if ( failCounter == 4 )
           {
               getWorld().removeObjects(getWorld().getObjects(Heart4.class));
           } // end if 
          
           if ( failCounter == 5 )
           {
               getWorld().removeObjects(getWorld().getObjects(Heart5.class));    
               
               
               getImage().setTransparency(0);
               
               drama.stop();
               drama.play();
               
               Greenfoot.stop();
               
               //getWorld().removeObject( this );
                   //removeObject ( thisShark ); // remove shark object from the world
               
                
           } // end if 
           
       } // end if
       
       else
       {
           setImage("Shark.png");
       } // end else
        
        if ( testCounter == 2 )
        {
            getWorld().removeObjects(getWorld().getObjects(Associate.class));
        } // end if
        
        if ( testCounter == 4 )
        {
            getWorld().removeObjects(getWorld().getObjects(Bachelor.class));
            
        } // end if 
        
        if ( testCounter == 8 )
        {
            getWorld().removeObjects(getWorld().getObjects(Masters.class));
            
        } // end if 
       
       
      
       
    } //end method
    
    /**
     * NOTE: This method is from crab game
     * 
     * Return true if we can see an object of class 'clss' right where we are. 
     * False if there is no such object here.
     */
    public boolean canSee(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        
        return actor != null;        
        
    } // end method
    
    
    
 
    /**
     *  NOTE: This method is from crab game
     *  
     * Try to eat an object of class 'clss'. This is only successful if there
     * is such an object where we currently are. Otherwise this method does
     * nothing.
     */
    public void eat(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        
        if(actor != null) 
        {
            getWorld().removeObject(actor);
        } // end if 
        
    } // end method
    
    /**
     *  This method detects whether or not the Shark has reached
     *  any boundaries of the world. If so, then the Shark turns by 15 
     *  degrees.
     */
    public void boundary()
    {
        if ( this.getX() > 700 ) // if shark reaches right boundary
        {
          turn(15); // turn 15 degrees
        } // end if 
        
        if ( this.getX() <= 0 ) // if shark reaches left boundary
        {
          turn(15);
        } // end if
        
        if ( this.getY() > 500 ) // if shark reaches bottom boundary
        {
            turn(15);
        } // end if
        
        if ( this.getY() <= 0 ) // if shark reaches top boundary
        {
            turn(15);
        } // end if
            
    } // end method boundary

  
    
    
    /**
     * When this method is called, it updates the actor object's
     * image to whatever is the next frame in the images array
     * 
     * if the imageIndex exceeds the highest index in the array,
     * then the imageIndex is reset to zero.
     * 
     */
    //public void updateImage()
    //{
        // imageIndex is the array index that corresponds 
        // to the current image that we want to display
      //  setImage( images[imageIndex] );
        
        // We pre-increment imageIndex, and then we check
        // to see if the incremented value is greater than 
        // the highest available index in the array
        // if ( ++imageIndex > (images.length-1) )
        //{
          //  imageIndex = 0;
       // } // end if
        
   /// } // end method updateImage
}
