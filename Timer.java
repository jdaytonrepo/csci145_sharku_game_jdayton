import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class Timer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Timer extends Text
{
    private int gameTime; // number of game-time units elapsed  
    private int gameTimer; // frame counter
    private int minute;
    private int hour;
    /**
     * Act - do whatever the Timer wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (getWorld() != null)  
        {  
            gameTimer = (gameTimer + 1) % 60; // adjust 6 to suit  
            if (gameTimer == 0)
            {
                gameTime++; 
            }
        } // end if 
        
        if ( gameTime > 59 )
        {
            gameTime = 0;
            minute += 1;
        } // end if 
        
        if ( minute > 59 )
        {
            gameTime = 0;
            minute = 0;
            hour += 1;
        } // end if 
        
        setImage( new GreenfootImage( "Time : " + hour + " : " + minute + " : " + gameTime, 24, Color.GREEN, Color.BLUE));
        
    }    
}
